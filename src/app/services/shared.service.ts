import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private _sharedMap: Map<string, any> = new Map<string, any>();

  constructor() { }

  get(key: string): any {
    return this._sharedMap.get(key);
  }

  set(key: string, value: any) {
    this._sharedMap.set(key, value);
  }
}
