import { Injectable } from '@angular/core';
import {HttpService} from './http.service';
import {NGXLogger} from 'ngx-logger';
import {Observable, of} from 'rxjs';
import {catchError, flatMap} from 'rxjs/operators';
import {SECTOR_URL} from '../utils/constants/endpoints';

@Injectable({
  providedIn: 'root'
})
export class SectorService {

  constructor(private _httpService: HttpService,
              private _logger: NGXLogger) { }

  get(): Observable<Sector[]> {
    return this._httpService.get(SECTOR_URL)
      .pipe(
        flatMap((data: any) => {
          this._logger.info('Recieved data', data);
          return of(data);
        }),
        catchError((error) => {
          this._logger.error('Cannot retrieve data', error);
          return of([]);
        })
      );
  }
}
