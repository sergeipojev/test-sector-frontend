import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  private _status: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _activeSpinner: number;

  constructor() {
    this._activeSpinner = 0;
  }

  public get status(): BehaviorSubject<boolean> {
    return this._status;
  }

  public start(): void {
    this._activeSpinner += 1;
    this._status.next(true);
  }

  public stop(): void {
    if (this._activeSpinner > 0) {
      this._activeSpinner -= 1;
    }

    if (this._activeSpinner === 0) {
      this._status.next(false);
    }
  }
}
