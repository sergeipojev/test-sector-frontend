import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpService} from './http.service';
import {NGXLogger} from 'ngx-logger';
import {catchError, flatMap} from 'rxjs/operators';
import {REGISTRATION_URL} from '../utils/constants/endpoints';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private _httpService: HttpService,
              private _logger: NGXLogger) { }

  get(id: number): Observable<Registration> {
    return this._httpService.get(REGISTRATION_URL + '/' + id)
      .pipe(
        flatMap((data: any) => {
          this._logger.info('Recieved data', data);
          return of(data);
        }),
        catchError((error) => {
          this._logger.error('Cannot retrieve data', error);
          return of([]);
        })
      );
  }

  post(registration: Registration): Observable<Registration> {
    return this._httpService.post(REGISTRATION_URL, registration)
      .pipe(
        flatMap((data: any) => {
          this._logger.info('Data saved', data);
          return of(data);
        }),
        catchError((error) => {
          this._logger.error('Cannot retrieve data', error);
          return of([]);
        })
      );
  }

  put(registration: Registration): Observable<Registration> {
    return this._httpService.put(REGISTRATION_URL, registration)
      .pipe(
        flatMap((data: any) => {
          this._logger.info('data updated', data);
          return of(data);
        }),
        catchError((error) => {
          this._logger.error('Cannot retrieve data', error);
          return of([]);
        })
      );
  }
}
