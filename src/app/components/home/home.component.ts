import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {NGXLogger} from 'ngx-logger';
import {SpinnerService} from '../../services/spinner.service';
import {takeUntil} from 'rxjs/operators';
import {Utils} from '../../utils/utils';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SectorService} from '../../services/sector.service';
import {ToasterService} from 'angular2-toaster';
import {RegistrationService} from '../../services/registration.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  preserveWhitespaces: true
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  private _unsubscribe: Subject<void> = new Subject<void>();

  form: FormGroup;

  sectorsList: Sector[];
  savedRegistration: Registration;
  editMode = false;

  constructor(private _logger: NGXLogger,
              private _spinner: SpinnerService,
              private _sectorService: SectorService,
              private _registrationService: RegistrationService,
              private _toasterService: ToasterService) {
    this._spinner.start();
    this._createForm();
  }

  ngOnInit() {
    this._sectorService.get()
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((data) => {
          this._logger.info('Sectors retrieved', data);
          this.sectorsList = data;
        },
        (error) => {
          this._logger.error('Error appeared', error);
          this._spinner.stop();
          this._toasterService.pop('error', 'Something went wrong.', 'Something went wrong. Please try again.');
        });
  }

  ngAfterViewInit(): void {
    this._spinner.stop();
  }

  onSubmit() {
    this._logger.info(this.form.value);
    if (this.editMode) {
      this._updateData();
    } else {
      this._saveData();
    }
  }

  private _createForm() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      sectors: new FormControl([], Validators.required),
      termsConditions: new FormControl(false, Validators.requiredTrue)
    });
  }

  private _updateData() {
    this.form.addControl('id', new FormControl(this.savedRegistration.id));
    this._registrationService.put(this.form.value)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((data) => {
          this._logger.info('Registration saved', data);
          this._toasterService.pop('success', 'Registration updated', 'Your registration has beed updated.');
          this.savedRegistration = data;
        },
        (error) => {
          this._logger.error('Error appeared', error);
          this._toasterService.pop('error', 'Something went wrong.', 'Something went wrong. Please try again.');
        });
  }

  private _saveData() {
    this._registrationService.post(this.form.value)
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((data) => {
          this._logger.info('Registration saved', data);
          this._toasterService.pop('success', 'Registration saved', 'Your registration has been saved.');

          this.savedRegistration = data;
          this.editMode = true;
        },
        (error) => {
          this._logger.error('Error appeared', error);
          this._toasterService.pop('error', 'Something went wrong.', 'Something went wrong. Please try again.');
        });
  }

  ngOnDestroy(): void {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }
}
