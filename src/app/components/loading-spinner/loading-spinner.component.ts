import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SpinnerService} from '../../services/spinner.service';

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.css']
})
export class LoadingSpinnerComponent implements OnInit, OnDestroy {

  // Examples http://tobiasahlin.com/spinkit/

  @Input()
  active = false;

  private _unsubscribe: Subject<void> = new Subject<void>();

  constructor(private _spinner: SpinnerService) { }

  ngOnInit(): void {
    this._spinner.status
      .pipe(takeUntil(this._unsubscribe))
      .subscribe((status: boolean) => this.active = status);
  }

  ngOnDestroy(): void {
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }
}
