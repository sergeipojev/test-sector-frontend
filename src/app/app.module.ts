import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LoadingSpinnerComponent} from './components/loading-spinner/loading-spinner.component';
import {ToasterModule} from 'angular2-toaster';
import {DatePipe} from '@angular/common';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import { HomeComponent } from './components/home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    LoadingSpinnerComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxWebstorageModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    ToasterModule.forRoot(),
    LoggerModule.forRoot({
      level: NgxLoggerLevel.TRACE
    }),
    FormsModule,
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
