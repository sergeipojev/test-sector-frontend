export class Utils {

  static isEmpty(object: any): boolean {
    return !object
      || (typeof object === 'string' && object.trim().length === 0)
      || (typeof object !== 'number' && !(object instanceof Date) && Object.keys(object).length === 0);
  }
}
