interface Sector {
  id: number;
  name: string;
  parentId: number;
  code: number;
  indentinationCount: number;
}
