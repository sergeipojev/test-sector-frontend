interface Registration {
  id: number;
  name: string;
  termsConditions: boolean;
  sectors: Sector[];
}
